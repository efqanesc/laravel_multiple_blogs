<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\AdminDashboardController;
use App\Http\Controllers\PostController;
use App\Http\Controllers\BlogController;
use App\Http\Controllers\AdminAuthController;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::post('/language/change',[BlogController::class,'language'])->name("language");

Route::get("admin/login",[AdminAuthController::class,"login"])->name("admin.login");
Route::post("admin/login",[AdminAuthController::class,"loginPost"]);
//Route::get("admin/register",[AdminAuthController::class,"register"]);
//Route::post("admin/register",[AdminAuthController::class,"registerPost"]);


Route::group(["middleware"=>"auth:admin"],function (){
    Route::get('/admin/category', [AdminDashboardController::class,"category"])->name("admin.category");
    Route::post('/category/store', [AdminDashboardController::class,"store"]);
    Route::post('/category/show', [AdminDashboardController::class,"show"]);
    Route::post('/category/delete', [AdminDashboardController::class,"delete"]);

    Route::get('/admin', [AdminDashboardController::class,"index"])->name("admin.dashboard");

    Route::get('/admin/post', [PostController::class,"post"])->name("admin.post");
    Route::post('/post/store', [PostController::class,"store"]);
    Route::post('/post/show', [PostController::class,"show"]);
    Route::post('/post/delete', [PostController::class,"delete"]);

    Route::get("admin/logout",[AdminAuthController::class,"logout"]);
});







Route::post('image/upload',[PostController::class,'upload']);

Auth::routes();
Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');


Route::group(['prefix' => '{locale?}', 'middleware' => 'localize'], function () {
    Route::get('/',[BlogController::class,'index']);
    Route::get("/category/{category}",[BlogController::class,'category']);
    Route::get('/blog/{slug}',[BlogController::class,'show']);
    Route::get('/search/',[BlogController::class,'search']);
});






