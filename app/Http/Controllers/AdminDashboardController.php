<?php

namespace App\Http\Controllers;

use App\Models\Category;
use App\Models\CategoryTranslate;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

class AdminDashboardController extends Controller
{
    public function index(){

        return view("admin.dashboard");
    }

    public function delete(Request $request){
        $category=Category::find($request->id);
        $category->translate()->delete();
        $category->delete();

        $categories=Category::with('translate')->get();
        $language=$request->language;
        $view=view("admin.partials.categoryTable",compact("categories", "language"))->render();
        return response()->json([
            "view"=>$view
        ],200);

    }


    public function category(){
        $categories=Category::with("translate")->get();
        return view('admin.category.category',compact('categories'));
    }

    public function store(Request $request){






        $check=true;

        foreach ($request->all()["name"] as $v){
            if(!is_null($v)){
                $check=false;
            }
        }

        if($check){
            return response()->json([
                "errors"=>[
                    "name"=>["Title en az bir dilde olmalidir"],
                ]
            ],422);
        }




        Validator::extend('except_exists', function($attribute, $value, $parameters)
        {
            if($value==$parameters[2]){
                return true;
            }
            return DB::table($parameters[0])
                    ->where($parameters[1], '=', $value)
                    ->count()>0;
        });


        $this->validate($request,[
            "id"=>"required|integer|except_exists:categories,id,0"
        ],[ "except_exists"=>":attribute bazada movcud deyil",]);



        if($request->id==0){
            $cat=new Category();
            $cat->save();
            $datas=array_filter($request->all()["name"]);
            foreach ($datas as $key=>$val){
                $cat->translate()->create([
                    "language_id"=>$key,
                    "title"=>$val
                ]);
            }
        }else{
            $cat=Category::find($request->id);
            $datas=array_filter($request->all()["name"]);
            $cat->translate()->delete();
            foreach ($datas as $key=>$val){
                $cat->translate()->create([
                    "language_id"=>$key,
                    "title"=>$val
                ]);
            }
        }
        $categories=Category::with('translate')->get();
        $language=$request->language;
        $view=view("admin.partials.categoryTable",compact("categories", "language"))->render();
        return response()->json([
            "view"=>$view
        ],200);
    }


    public function show(Request $request){

        $category=Category::find($request->id);
        $language=$request->language;
        $view=view("admin.partials.categoryModal",compact("category", "language"))->render();
        return response()->json([
            "view"=>$view
        ],200);

    }
}
