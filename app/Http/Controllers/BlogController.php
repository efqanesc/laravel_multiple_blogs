<?php

namespace App\Http\Controllers;

use App\Models\Blog;
use App\Models\BlogTranslate;
use App\Models\Category;
use App\Models\CategoryTranslate;
use Illuminate\Http\Request;

class BlogController extends Controller
{


    public function localeEqualId($locale)
    {
        $arr = ["az" => 1, "en" => 2, "ru" => 3];
        return $arr[$locale];
    }

    public function index(string $locale)
    {
        $categories = CategoryTranslate::whereNotNull("title")
            ->where("language_id", $this->localeEqualId(app()->getLocale()))
            ->get();
        $blogs = Blog::with(["translate" => function ($query) use ($locale) {
            return $query->whereNotNull("title")
                ->where("language_id", $this->localeEqualId($locale));
        }])
            ->paginate(10);
        return view("front.blog", compact("blogs", "categories"));
    }

    public function category($locale, $category)
    {
        $category = CategoryTranslate::find($category)->category;
        $blogs = $category->blogs()
            ->with(["translate" => function ($query) use ($locale) {
                return $query->where("language_id", $this->localeEqualId($locale));
            }])
            ->paginate(10);
        $categories = CategoryTranslate::where("language_id", $this->localeEqualId(app()->getLocale()))
            ->get();
        return view('front.blog', compact('blogs', 'categories'));
    }

    public function show($locale, $slug)
    {
        $blog_translate = BlogTranslate::where("slug", $slug)
            ->with("blog")
            ->first();
        $categories = CategoryTranslate::where("language_id", $this->localeEqualId(app()->getLocale()))
            ->get();

        return view("front.blog_single", compact("blog_translate", "categories"));
    }

    public function language(Request $request)
    {

        $urlmain = request()->url;

        $url = trim(parse_url($urlmain)['path'], "/");
        $url = explode("/", $url);



        if (count($url) > 1 && $url[1] == "blog") {
            $blog = BlogTranslate::whereSlug($url[2])
                ->first()->blog;
            $slug = $blog->translate()
                ->where("language_id", $this->localeEqualId($request->language))
                ->first()->slug;
            $url[2] = $slug;
        }

        $url[0] = $request->language;


        return response()->json([
            "url" => url(implode("/", $url)),
        ]);
    }


    public function search($locale, Request $request)
    {
        $search = $request->search;

        $categories = CategoryTranslate::whereNotNull("title")
            ->where("language_id", $this->localeEqualId(app()->getLocale()))
            ->get();



        $blogs=BlogTranslate::where("language_id", $this->localeEqualId($locale))
               ->where(function ($query) use ($search){
                    $query ->where("title", 'like', '%'.$search.'%')
                        ->orwhere("subtitle", 'like', '%'.$search.'%')
                        ->orWhere("content", 'like', '%'.$search.'%');
            })


            ->paginate(10);



        return view("front.search", compact("blogs", "categories"));

    }
}
