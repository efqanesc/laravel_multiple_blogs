<?php

namespace App\Http\Controllers;

use App\Models\Blog;
use App\Models\Category;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

class PostController extends Controller
{
    public function post(){

        $posts=Blog::with("translate")->get();
        $categories=Category::all();
        return view('admin.post.post',compact('posts','categories'));
    }

    public function store(Request $request){


        $check=true;
        $datas=$request->all();




        $arr=[];
        foreach([1,2,3] as $key){
            $check=true;
            foreach(["title","slug","content","subtitle"] as $val){
                if(is_null($datas[$val][$key])){
                    $check=false;
                }
            }
            if(strlen($datas['subtitle'][$key])>255){
                return response()->json([
                    "errors"=>[
                        "subtitle"=>["subtitle 255 simvol ola biler"],
                    ]
                ],422);
            }
            $arr[$key]=$check;
        }



        if(!in_array(true,$arr)){

            return response()->json([
                "errors"=>[
                    "name"=>["En az bir dilde butun sutunlar doldurulmalidir"],
                ]
            ],422);
        }




        if($request->id==0){

            $this->validate($request,[
                "image"=>"required|image"
            ]);

            $cat=new Blog();
            $cat->category_id=$request->category_id;
            $imageName=uniqid().".".$request->image->getClientOriginalExtension();
            $request->image->move("images",$imageName);
            $cat->image=$imageName;
            $cat->save();

            $datas=$request->all();
            foreach ([1,2,3] as $v){
                $cat->translate()->create([
                    "language_id"=>$v,
                    "title"=>$datas["title"][$v],
                    "content"=>$datas["content"][$v],
                    "slug"=>Str::slug($datas["slug"][$v]),
                    "subtitle"=>$datas["subtitle"][$v],
                ]);
            }
        }else{
            $post=Blog::find($request->id);
            $post->category_id=$request->category_id;
            if($request->hasFile("image")){
                if(file_exists(public_path('images/'.$post->image))){
                    unlink(public_path('images/'.$post->image));
                }

                $imageName=uniqid().".".$request->image->getClientOriginalExtension();

                $request->image->move("images",$imageName);

                $post->image=$imageName;
            }
            $post->save();

            $datas=$request->all();
            $post->translate()->delete();
            foreach ([1,2,3] as $v){
                $post->translate()->create([
                    "language_id"=>$v,
                    "title"=>$datas["title"][$v],
                    "content"=>$datas["content"][$v],
                    "slug"=>Str::slug($datas["slug"][$v]),
                    "subtitle"=>$datas["subtitle"][$v],
                ]);
            }
        }
        $posts=Blog::with('translate')->get();
        $language=$request->language;
        $view=view("admin.partials.blogTable",compact("posts", "language"))->render();
        return response()->json([
            "view"=>$view
        ],200);
    }

    public function show(Request $request){

        $post=Blog::find($request->id);
        $language=$request->language;


        $contents=[];

        foreach ($post->translate as $key=>$item) {
            $contents[$item->language_id]=html_entity_decode($item->content);
        }


        $categories=Category::with('translate')->get();
        $view=view("admin.partials.postModal",compact("post", "language",'categories'))->render();

        return response()->json([
            "view"=>$view,
            "contents"=>$contents
        ],200);

    }


    public function upload(Request $request){
        $image=$request->image;
        $imageName=uniqid().'.'.$image->getClientOriginalExtension();
        $image->move(public_path('images'),$imageName);
        return response()->json([
            'url'=>asset('images/'.$imageName),
        ]);
    }


    public function delete(Request $request){
        $post=Blog::find($request->id);

        if(file_exists(public_path('images/'.$post->image))){
            unlink(public_path('images/'.$post->image));
        }

        $post->translate()->delete();
        $post->delete();

        $posts=Blog::with('translate')->get();
        $language=$request->language;
        $view=view("admin.partials.blogTable",compact("posts", "language"))->render();
        return response()->json([
            "view"=>$view
        ],200);

    }
}
