<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBlogTranslatesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('blog_translates', function (Blueprint $table) {
            $table->id();
            $table->tinyInteger("language_id");
            $table->unsignedBigInteger("blog_id");
            $table->string("slug")->nullable();
            $table->string("title")->nullable();
            $table->longText("content")->nullable();
            $table->foreign("blog_id")->references("id")->on("blogs")->onDelete("cascade");
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('blog_translates');
    }
}
