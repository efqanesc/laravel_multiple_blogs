

<!DOCTYPE html>
<html lang = "en">
<head>
    <meta charset = "utf-8">
    <meta name = "viewport" content = "width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>AdminLTE 3 | Dashboard</title>
    <!-- Google Font: Source Sans Pro -->
    <link rel = "stylesheet"
            href = "https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=fallback">
    <!-- Font Awesome -->
    <link rel = "stylesheet" href = "{{asset('plugins/fontawesome-free/css/all.min.css')}}">
    <!-- Ionicons -->
    <link rel = "stylesheet" href = "https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
    <!-- Tempusdominus Bootstrap 4 -->
    {{--<link rel = "stylesheet"--}}
            {{--href = "{{asset("plugins/tempusdominus-bootstrap-4/css/tempusdominus-bootstrap-4.min.css")}}">--}}
    <!-- iCheck -->
    <!-- JQVMap -->

    <!-- Theme style -->
    <link rel = "stylesheet" href = "{{asset('dist/css/adminlte.min.css')}}">
    <!-- overlayScrollbars -->
    <link rel = "stylesheet" href = "{{asset('plugins/overlayScrollbars/css/OverlayScrollbars.min.css')}}">
    <!-- Daterange picker -->

    <!-- summernote -->

    @stack("css")
</head>
<body class = "hold-transition sidebar-mini layout-fixed">
<div class = "wrapper">
    <!-- Preloader -->
{{--<div class="preloader flex-column justify-content-center align-items-center">--}}
{{--<img class="animation__shake" src="dist/img/AdminLTELogo.png" alt="AdminLTELogo" height="60" width="60">--}}
{{--</div>--}}

<!-- Navbar -->
    <nav class = "main-header navbar navbar-expand navbar-white navbar-light">
        <!-- Left navbar links -->
        <ul class = "navbar-nav">
            <li class = "nav-item">
                <a class = "nav-link" data-widget = "pushmenu" href = "#" role = "button"><i class = "fas fa-bars"></i></a>
            </li>
            <li class = "nav-item d-none d-sm-inline-block">
                <a href = "index3.html" class = "nav-link">Home</a>
            </li>
            <li class = "nav-item d-none d-sm-inline-block">
                <a href = "#" class = "nav-link">Contact</a>
            </li>
        </ul>
        <!-- Right navbar links -->
        <ul class = "navbar-nav ml-auto">
            <!-- Navbar Search -->


            <!-- Notifications Dropdown Menu -->
            <li class = "nav-item dropdown">
                <a class = "nav-link" data-toggle = "dropdown" href = "#">

                    {{Auth::guard("admin")->user()->name}}
                    <span class = "badge badge-warning navbar-badge"></span>
                </a>
                <div class = "dropdown-menu dropdown-menu-lg dropdown-menu-right">
                    <div class = "dropdown-divider"></div>
                    <a href = "/admin/logout" class = "dropdown-item">
                        <i class = "fas fa-envelope mr-2"></i> Logout
                        <span class = "float-right text-muted text-sm"></span>
                    </a>

                </div>
            </li>

            <li class = "nav-item">
                <a class = "nav-link" data-widget = "control-sidebar" data-slide = "true" href = "#" role = "button">
                    <i class = "fas fa-th-large"></i>
                </a>
            </li>
        </ul>
    </nav>
    <!-- /.navbar -->
    <!-- Main Sidebar Container -->
@include("admin.sidebar")

<!-- Content Wrapper. Contains page content -->
    <div class = "content-wrapper">
        <!-- Content Header (Page header) -->
        <div class = "content-header">
            <div class = "container-fluid">
                <div class = "row mb-2">

                </div>
            </div>
        </div>
    @yield("content")

    </div>
    <!-- /.content-wrapper -->
    <footer class = "main-footer">
        <strong>Copyright &copy; 2014-2021 <a href = "https://adminlte.io">AdminLTE.io</a>.</strong>
        All rights reserved.
        <div class = "float-right d-none d-sm-inline-block">
            <b>Version</b> 3.1.0
        </div>
    </footer>
    <!-- Control Sidebar -->
    <aside class = "control-sidebar control-sidebar-dark">
        <!-- Control sidebar content goes here -->
    </aside>
    <!-- /.control-sidebar -->
</div>
<!-- ./wrapper -->
<!-- jQuery -->
<script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
<!-- jQuery UI 1.11.4 -->
<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<script>

</script>
<!-- Bootstrap 4 -->
<script src = "{{asset('plugins/bootstrap/js/bootstrap.bundle.min.js')}}"></script>
<!-- ChartJS -->

<!-- Sparkline -->

<!-- JQVMap -->

<!-- jQuery Knob Chart -->

<!-- Tempusdominus Bootstrap 4 -->
{{--<script src = "{{asset('plugins/tempusdominus-bootstrap-4/js/tempusdominus-bootstrap-4.min.js')}}"></script>--}}
<!-- Summernote -->

<!-- overlayScrollbars -->
<script src = "{{asset('plugins/overlayScrollbars/js/jquery.overlayScrollbars.min.js')}}"></script>
<!-- AdminLTE App -->
<script src = "{{asset('dist/js/adminlte.js')}}"></script>
<!-- AdminLTE for demo purposes -->
<script src = "{{asset('dist/js/demo.js')}}"></script>
<!-- AdminLTE dashboard demo (This is only for demo purposes) -->

<script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>



@stack("scripts")






</body>
</html>
