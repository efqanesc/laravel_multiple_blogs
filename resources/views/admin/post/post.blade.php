@extends("admin.layout")


@push("css")
    <link href = "//cdn.quilljs.com/1.2.2/quill.snow.css" rel = "stylesheet">

    <style>
        .ql-editor {
            min-height: 200px;
        }

        .select2-selection__rendered {
            line-height: 31px !important;
        }
        .select2-container .select2-selection--single {
            height: 40px !important;
        }
        .select2-selection__arrow {
            height: 34px !important;
        }
    </style>
    <link rel = "stylesheet" href = "https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.13/css/select2.min.css"
            integrity = "sha512-nMNlpuaDPrqlEls3IX/Q56H36qvBASwb3ipuo3MxeWbsQB1881ox0cRv7UPTgBlriqoynt35KjEwgGUeUXIPnw=="
            crossorigin = "anonymous" referrerpolicy = "no-referrer"/>

@endpush

@section('content')
    <section class = "content">
        <div class = "container-fluid">
            <div class = "row">
                <div class = "col-md-12">
                    <div class = "card">
                        <div class = "card-header">
                            <h3 class = "card-title">Bordered Table</h3>
                        </div>
                        <!-- /.card-header -->
                        <div class = "card-body">
                            <select name = "" class = "form-control"
                                    style = "margin: 20px 0px; width: 100px;display: inline" id = "tabelchange">
                                <option value = "language_1">Az</option>
                                <option value = "language_2">En</option>
                                <option value = "language_3">Ru</option>
                            </select>
                            <button onclick = "resetForm()" style = "margin: 20px" type = "button"
                                    class = "btn btn-primary" data-toggle = "modal" data-target = "#modal-default">
                                Create Post
                            </button>
                            <div class = "modal fade" id = "modal-default" data-id = "0">
                                <div class = "modal-dialog">
                                    <div class = "modal-content">
                                        <div class = "modal-header">
                                            <h4 class = "modal-title" style = "width: 100%">Create</h4>
                                            <select name = "" id = "modalchange" class = "form-control"
                                                    style = "width: 100px;">
                                                <option value = "tabs_1">Az</option>
                                                <option value = "tabs_2">En</option>
                                                <option value = "tabs_3">Ru</option>
                                            </select>
                                        </div>
                                        <form id = "form">
                                            <div class = "modal-body">
                                                <div class = "form-group tabs_1 tabs">
                                                    <label for = "">Title Az</label>
                                                    <input type = "text" name = "title[1]" class = "form-control">
                                                </div>
                                                <div class = "form-group tabs_1 tabs">
                                                    <label for = "">Slug Az</label>
                                                    <input type = "text" name = "slug[1]" class = "form-control">
                                                </div>
                                                <div class = "form-group tabs_1 tabs">
                                                    <label for = "">Content Az</label>
                                                    <textarea name = "content[1]" type = "text"
                                                            style = "display: none;"></textarea>
                                                    <div class = "quilleditor"></div>
                                                </div>
                                                <div class = "form-group tabs_1 tabs">
                                                    <label for = "">Subtitle Az</label>
                                                    <textarea style="height: 100px;" name = "subtitle[1]" class="form-control" id = "" cols = "30" rows = "10"></textarea>

                                                </div>
                                                <div class = "form-group tabs_2 tabs">
                                                    <label for = "">Title En</label>
                                                    <input type = "text" name = "title[2]" class = "form-control">
                                                </div>
                                                <div class = "form-group tabs_2 tabs">
                                                    <label for = "">Slug En</label>
                                                    <input type = "text" name = "slug[2]" class = "form-control">
                                                </div>
                                                <div class = "form-group tabs_2 tabs">
                                                    <label for = "">Content En</label>
                                                    <textarea style = "display: none;" name = "content[2]" id = ""
                                                            cols = "30" rows = "10"></textarea>
                                                    <div class = "quilleditor"></div>
                                                </div>
                                                <div class = "form-group tabs_2 tabs">
                                                    <label for = "">Subtitle En</label>
                                                    <textarea style="height: 100px;" name = "subtitle[2]" class="form-control" id = "" cols = "30" rows = "10"></textarea>
                                                </div>
                                                <div class = "form-group tabs_3 tabs">
                                                    <label for = "">Title Ru</label>
                                                    <input type = "text" name = "title[3]" class = "form-control">
                                                </div>
                                                <div class = "form-group tabs_3 tabs">
                                                    <label for = "">SLug Ru</label>
                                                    <input type = "text" name = "slug[3]" class = "form-control">
                                                </div>

                                                <div class = "form-group tabs_3 tabs">
                                                    <label for = "">Content Ru</label>
                                                    <textarea style = "display: none;" name = "content[3]" id = ""
                                                            cols = "30" rows = "10"></textarea>
                                                    <div class = "quilleditor"></div>
                                                </div>
                                                <div class = "form-group tabs_3 tabs">
                                                    <label for = "">Subtitle Ru</label>
                                                    <textarea style="height: 100px;" name = "subtitle[3]" class="form-control" id = "" cols = "30" rows = "10"></textarea>

                                                </div>
                                                <div class = "form-group">
                                                    <label for = "">Category</label>
                                                    <select class="select2" style="width: 100%" name = "category_id" id = "category_id">
                                                        @foreach($categories as $c)
                                                            <option value = "{{$c->id}}">{{$c->translate()->first()->title}}</option>
                                                            @endforeach
                                                    </select>
                                                </div>



                                                <div class = "form-group">
                                                    <label for = "">Image</label>
                                                    <input type = "file" id = "image" name = "image"
                                                            class = "form-control">
                                                </div>
                                            </div>
                                            <div class = "modal-footer justify-content-between">
                                                <button type = "button" class = "btn btn-default"
                                                        data-dismiss = "modal">Close
                                                </button>
                                                <button type = "submit" class = "btn btn-primary">Save changes</button>
                                            </div>
                                        </form>
                                    </div>
                                    <!-- /.modal-content -->
                                </div>
                                <!-- /.modal-dialog -->
                            </div>
                            <div class = "table-responsive">
                                <table class = "table table-bordered" id = "dataTable">
                                    <thead>
                                    <tr>
                                        <th style = "width: 10px">#</th>
                                        <th>Slug</th>
                                        <th>Title</th>
                                        <th>Subtitle</th>
                                        <th>Content</th>
                                        <th>Category</th>
                                        <th>Image</th>
                                        <th>Options</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($posts as $cat)


                                        @foreach( $cat->translate as $t)


                                            <tr style = "display: none"
                                                    class = "languages language_{{$t->language_id}}">
                                                <td></td>
                                                <td>{{substr($t->slug,0,100)}}</td>
                                                <td>{{substr($t->title,0,100)}}</td>
                                                <td>{{$t->subtitle}}</td>
                                                <td>{{substr($t->content,0,100)}}</td>
                                                <td>{{ $t->blog->category->translate()->where("language_id",$t->language_id)->first()->title }}</td>
                                                <td><img style = "width: 150px;" src = "/images/{{$cat->image}}"></td>
                                                <td >
                                                    <button class = "btn btn-sm btn-danger float-left"
                                                            onclick = "deleteCat('{{$cat->id}}')">D
                                                    </button>
                                                    <button class = "btn btn-sm btn-success float-right"
                                                            onclick = "edit('{{$cat->id}}')">E
                                                    </button>
                                                </td>
                                            </tr>

                                        @endforeach



                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                            <ul class = "pagination pagination-sm m-0 float-right my-2"></ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

@endsection

@push("scripts")





    <script>
        $(function () {

            $(".select2").select2()

            resetForm = () => {
                console.log("isledi")
                $("input").val('')

                $(".ql-editor").html('')
                $("textarea").html("")
                $("body .imageGroup").html(`
                 <label for = "">Image</label>
                <input type = "file" id="image" name = "image" class = "form-control">
                `)
            }


            var toolbarOptions = [
                ['bold', 'italic', 'underline', 'strike'],        // toggled buttons
                ['blockquote', 'code-block'],

                [{'header': 1}, {'header': 2}],               // custom button values
                [{'list': 'ordered'}, {'list': 'bullet'}],
                [{'script': 'sub'}, {'script': 'super'}],      // superscript/subscript
                [{'indent': '-1'}, {'indent': '+1'}],          // outdent/indent
                [{'direction': 'rtl'}],                         // text direction
                [{'size': ['small', false, 'large', 'huge']}],  // custom dropdown
                [{'header': [1, 2, 3, 4, 5, 6, false]}],
                [{'color': []}, {'background': []}],          // dropdown with defaults from theme
                [{'font': []}],
                [{'align': []}],
                ['link', 'image', 'video'],          // add's image support
                ['clean']
            ];


            function quill(contents = undefined) {

                console.log(contents)

                let containers = document.querySelectorAll('.quilleditor');
                let editors = Array.from(containers).map(function (container) {
                    return new Quill(container, {
                        modules: {
                            syntax: false,
                            toolbar: toolbarOptions,
                            imageResize: {
                                displaySize: true
                            },
                        },
                        theme: 'snow'
                    });
                });


                $.each(editors, function (key, editor) {


                    // console.log(e.descriptions)


                    if (contents != undefined) {
                        $(".quilleditor:eq(" + (key) + ") .ql-editor").html(contents[key + 1]);
                    }


                    editor.getModule('toolbar').addHandler('image', () => {
                        selectLocalClassImage(editor)
                    });


                    editor.on('text-change', function (delta, oldDelta, source) {
                        console.log("salam")
                        $(".quilleditor:eq(" + key + ")").siblings('textarea').text($(".quilleditor:eq(" + key + ") .ql-editor").html())
                    });

                })


                function selectLocalClassImage(quilleditor) {
                    const input = document.createElement('input');
                    input.setAttribute('type', 'file');
                    input.click();

                    // Listen upload local image and save to server
                    input.onchange = () => {
                        const file = input.files[0];

                        // file type is only image.
                        if (/^image\//.test(file.type)) {
                            imageHandlerClass(file, quilleditor);
                        } else {
                            console.warn('You could only upload images.');
                        }
                    };
                }

                function imageHandlerClass(image, quilleditor) {
                    var formData = new FormData();
                    formData.append('image', image);
                    formData.append('_token', '{{csrf_token()}}');


                    $.ajax({
                        type: "POST",
                        url: '/image/upload',
                        data: formData,
                        processData: false,
                        contentType: false,
                        success: function (response) {
                            if (response.url) {
                                insertToEditorClass(response.url, quilleditor);
                            }
                        }
                    });
                }

                function insertToEditorClass(url, editor) {
                    const range = editor.getSelection();
                    editor.insertEmbed(range.index, 'image', url);
                }


            }

            quill()


            $(".pagination").on("click", ".page-item", function () {
                var page = $(this).text().trim()

            })


            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': "{{csrf_token()}}"
                }
            });

            edit = (id) => {
                $.ajax({
                    url: "/post/show",
                    method: "post",
                    data: {"id": id, "language": $("#modalchange").val()},
                    success: function (e) {
                        $("#modal-default").html(e.view)
                        $(".modal").modal("show")
                        $(".modal").attr("data-id", id)
                        $(".select2").select2()
                        $(".tabs_1").show()
                        quill(e.contents)
                    },
                    error: function (e) {
                        console.log(e)
                    }
                })
            }

            deleteCat = (id) => {
                $.ajax({
                    url: "/post/delete",
                    method: "post",
                    data: {"id": id, "language": $("#modalchange").val()},
                    success: function (e) {
                        $("#dataTable").html(e.view)
                        sirala(1)
                        Swal.fire({
                            position: 'top-end',
                            icon: 'success',
                            title: 'Deleted Successfully',
                            showConfirmButton: false,
                            timer: 1500
                        })
                    },
                    error: function (e) {
                        console.log(e)
                    }
                })
            }

            $(".tabs").hide()
            $(".tabs_1").show()
            $("body").on("change", "#modalchange", function () {
                $(".tabs").hide()
                $("." + this.value).show()
            })


            $("body").on("submit", "#form", function (e) {
                e.preventDefault();
                var formData = new FormData(this);
                formData.append("id", $(".modal").attr("data-id"))
                formData.append("language", $("#tabelchange").val())
                formData.append("category_id",$("#category_id").select2().val())

                if ($('#image')[0].files.length != 0) {
                    formData.append('image', $('#image')[0].files[0]);
                }


                $.ajax({
                    url: "/post/store",
                    processData: false,
                    contentType: false,
                    cache: false,
                    method: "post",
                    data: formData,
                    success: function (e) {
                        Swal.fire({
                            position: 'top-end',
                            icon: 'success',
                            title: 'Your work has been saved',
                            showConfirmButton: false,
                            timer: 1500
                        })
                        $("#dataTable").html(e.view)
                        $("form input").val('')
                        $(".modal").modal("hide")
                        $(".modal").attr("data-id", 0)
                        sirala(1)
                    },
                    error: function (e) {
                        var errors=JSON.parse(e.responseText).errors;
                        for(a in errors){
                            for(j in errors[a]){
                                Swal.fire({
                                    position: 'top-end',
                                    icon: 'error',
                                    title: errors[a][j],
                                    showConfirmButton: false,
                                    timer: 1500
                                })
                            }
                        }
                    }
                })

            })


            $(".language_1").show()

            $("#tabelchange").on("change", function () {
                $(".languages").hide()
                $("." + this.value).show()
                sirala(1)
            })


            var goster = 5;


            function sirala(page) {


                var lang = $("#tabelchange").val()

                var count = $("tbody tr." + lang).length;

                var page_count = Math.ceil(count / goster);

                var basla = (page - 1) * goster;
                var son = page * goster;

                $('.pagination').html('')

                for (var i = 1; i <= page_count; i++) {
                    $(".pagination").append(` <li class="page-item ` + (i == page ? "active" : "") + ` ">
                                        <a class="page-link " >${i}</a>
                                    </li>`);
                }

                $("." + lang).hide()

                for (var i = basla; i < son; i++) {
                    $("." + lang + ":eq('" + i + "')").show()
                }


            }

            sirala(1)


            $(".pagination").on("click", ".page-item", function () {
                var page = $(this).text().trim()

                sirala(page)
            })







        })
    </script>



    <script src = "//cdn.quilljs.com/1.2.2/quill.min.js"></script>
    <script src = "{{asset('js/resize.min.js')}}"></script>
    <script src = "https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.13/js/select2.min.js"
            integrity = "sha512-2ImtlRlf2VVmiGZsjm9bEyhjGW4dU7B6TNwh/hx/iSByxNENtj3WVE6o/9Lj4TJeVXPi4bnOIMXFIJJAeufa0A=="
            crossorigin = "anonymous" referrerpolicy = "no-referrer"></script>
@endpush