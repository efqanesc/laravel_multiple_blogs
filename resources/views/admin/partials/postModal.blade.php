<div class = "modal-dialog">
    <div class = "modal-content">
        <div class = "modal-header">
            <h4 class = "modal-title" style = "width: 100%">Create</h4>
            <select name = "" id = "modalchange" class = "form-control" style = "width: 100px;">
                <option value = "tabs_1">Az</option>
                <option value = "tabs_2">En</option>
                <option value = "tabs_3">Ru</option>
            </select>
            {{--<button type = "button" class = "close" data-dismiss = "modal"--}}
            {{--aria-label = "Close">--}}
            {{--<span aria-hidden = "true">&times;</span>--}}
            {{--</button>--}}
        </div>
        <form id = "form">
            <div class = "modal-body">
                <div class = "form-group tabs tabs_1">
                    <label for = "">Slug Az</label>
                    <input type = "text" name = "slug[1]" class = "form-control"
                            value = "{{ $post->translate()->where("language_id",1)->exists() ? $post->translate()->where("language_id",1)->first()->slug : ""}}">
                </div>
                <div class = "form-group tabs tabs_1">
                    <label for = "">Title Az</label>
                    <input type = "text" name = "title[1]" class = "form-control"
                            value = "{{ $post->translate()->where("language_id",1)->exists() ? $post->translate()->where("language_id",1)->first()->title : ""}}">
                </div>
                <div class = "form-group tabs tabs_1">
                    <label for = "">Content Az</label>
                    <textarea style = "display: none" name = "content[1]"
                            class = "form-control">{{ $post->translate()->where("language_id",1)->exists() ? $post->translate()->where("language_id",1)->first()->content : ""}}</textarea>
                    <div class = "quilleditor"></div>
                </div>

                <div class = "form-group tabs_1 tabs" style = "display: none;">
                    <label for = "">Subtitle Az</label>
                    <textarea style = "height: 100px;" name = "subtitle[1]" class = "form-control" id = "" cols = "30"
                            rows = "10">{{ $post->translate()->where("language_id",1)->exists() ? $post->translate()->where("language_id",1)->first()->subtitle : ""}}</textarea>
                </div>




                <div class = "form-group tabs tabs_2" style = "display: none;">
                    <label for = "">Slug En</label>
                    <input type = "text" name = "slug[2]" class = "form-control"
                            value = "{{ $post->translate()->where("language_id",2)->exists() ? $post->translate()->where("language_id",2)->first()->slug : ""}}">
                </div>
                <div class = "form-group tabs tabs_2" style = "display: none;">
                    <label for = "">Title En</label>
                    <input type = "text" name = "title[2]" class = "form-control"
                            value = "{{ $post->translate()->where("language_id",2)->exists() ? $post->translate()->where("language_id",2)->first()->title : ""}}">
                </div>
                <div class = "form-group tabs tabs_2" style = "display: none;">
                    <label for = "">Content En</label>
                    <textarea style = "display: none;" name = "content[2]"
                            class = "form-control">{{ $post->translate()->where("language_id",2)->exists() ? $post->translate()->where("language_id",2)->first()->content : ""}}</textarea>
                    <div class = "quilleditor"></div>
                </div>
                <div class = "form-group tabs_2 tabs" style = "display: none;">
                    <label for = "">Subtitle En</label>
                    <textarea style = "height: 100px;" name = "subtitle[2]" class = "form-control" id = "" cols = "30"
                            rows = "10">{{ $post->translate()->where("language_id",2)->exists() ? $post->translate()->where("language_id",2)->first()->subtitle : ""}}</textarea>
                </div>
                <div class = "form-group tabs tabs_3" style = "display: none;">
                    <label for = "">Slug Ru</label>
                    <input type = "text" name = "slug[3]" class = "form-control"
                            value = "{{ $post->translate()->where("language_id",3)->exists() ? $post->translate()->where("language_id",3)->first()->slug : ""}}">
                </div>
                <div class = "form-group tabs tabs_3" style = "display: none;">
                    <label for = "">Title Ru</label>
                    <input type = "text" name = "title[3]" class = "form-control"
                            value = "{{ $post->translate()->where("language_id",3)->exists() ? $post->translate()->where("language_id",3)->first()->title : ""}}">
                </div>
                <div class = "form-group tabs tabs_3" style = "display: none;">
                    <label for = "">Content Ru</label>
                    <textarea style = "display: none" name = "content[3]" class = "form-control">
                        {{ $post->translate()->where("language_id",3)->exists() ? $post->translate()->where("language_id",3)->first()->content : ""}}
                    </textarea>
                    <div class = "quilleditor"></div>
                </div>
                <div class = "form-group tabs_3 tabs" style = "display: none;">
                    <label for = "">Subtitle Ru</label>
                    <textarea style = "height: 100px;" name = "subtitle[3]" class = "form-control" id = "" cols = "30"
                            rows = "10">{{ $post->translate()->where("language_id",3)->exists() ? $post->translate()->where("language_id",3)->first()->subtitle : ""}}</textarea>
                </div>
                <div class = "form-group">
                    <label for = "">Category</label>
                    <select class = "select2" style = "width: 100%" name = "category_id" id = "category_id">
                        @foreach($categories as $c)
                            <option @if($post->category->id==$c->id) selected @endif value = "{{$c->id}}">{{$c->translate()->first()->title}}</option>
                        @endforeach
                    </select>
                </div>
                <div class = "form-group imageGroup">
                    <label for = "">Image</label>
                    <img src = "{{asset('images/'.$post->image)}}" style = "width: 50%; display: block" alt = "">
                    <input type = "file" name = "image" id = "image" class = "form-control">
                </div>
            </div>
            <div class = "modal-footer justify-content-between">
                <button type = "button" class = "btn btn-default" data-dismiss = "modal">Close
                </button>
                <button type = "submit" class = "btn btn-primary">Save changes</button>
            </div>
        </form>
    </div>
    <!-- /.modal-content -->
</div>