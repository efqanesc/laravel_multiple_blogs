<div class = "modal-dialog">
    <div class = "modal-content">
        <div class = "modal-header">
            <h4 class = "modal-title" style="width: 100%">Create</h4>
            <select name = "" id = "modalchange" class = "form-control" style="width: 100px;">
                <option value = "tabs_1">Az</option>
                <option value = "tabs_2">En</option>
                <option value = "tabs_3">Ru</option>
            </select>
            {{--<button type = "button" class = "close" data-dismiss = "modal"--}}
            {{--aria-label = "Close">--}}
            {{--<span aria-hidden = "true">&times;</span>--}}
            {{--</button>--}}
        </div>
        <form id = "form">
            <div class = "modal-body">
                <div class = "form-group tabs tabs_1" >
                    <label for = "">Title Az</label>

                    <input type = "text" name = "name[1]"  class = "form-control" value="{{ $category->translate()->where("language_id",1)->exists() ? $category->translate()->where("language_id",1)->first()->title : ""}}">
                </div>
                <div class = "form-group tabs tabs_2"  style="display: none;" >
                    <label for = "">Title En</label>
                    <input type = "text" name = "name[2]"  class = "form-control" value="{{ $category->translate()->where("language_id",2)->exists() ? $category->translate()->where("language_id",2)->first()->title : ""}}">
                </div>
                <div class = "form-group tabs tabs_3" style="display: none;" >
                    <label for = "">Title Ru</label>
                    <input type = "text" name = "name[3]"  class = "form-control" value="{{ $category->translate()->where("language_id",3)->exists() ? $category->translate()->where("language_id",3)->first()->title : ""}}">
                </div>
            </div>
            <div class = "modal-footer justify-content-between">
                <button type = "button" class = "btn btn-default"
                        data-dismiss = "modal">Close
                </button>
                <button type = "submit" class = "btn btn-primary">Save changes</button>
            </div>
        </form>
    </div>
    <!-- /.modal-content -->
</div>