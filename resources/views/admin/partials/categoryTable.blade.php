<thead>
<tr>
    <th style = "width: 10px">#</th>
    <th>Title</th>
    <th>options</th>
</tr>
</thead>
<tbody>
@foreach($categories as $cat)

    @foreach($cat->translate as $t)

        <tr  class="languages language_{{$t->language_id}}" @if($language!="language_".$t->language_id) style="display: none;" @endif >
            <td>{{$cat->id}}</td>
            <td>{{$t->title}}</td>
            <td>
                <button class="btn btn-sm btn-danger"  onclick="deleteCat('{{$cat->id}}')">Delete</button>
                <button class="btn btn-sm btn-success" onclick="edit('{{$cat->id}}')">Edit</button>
            </td>
        </tr>

    @endforeach


@endforeach
</tbody>