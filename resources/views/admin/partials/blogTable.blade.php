<thead>
<tr>
    <th style = "width: 10px">#</th>
    <th>Slug</th>
    <th>Title</th>
    <th>Subtitle</th>
    <th>Content</th>
    <th>Category</th>
    <th>Image</th>
    <th>Options</th>
</tr>
</thead>
<tbody>
@foreach($posts as $cat)

    @foreach($cat->translate as $t)

        <tr style="display: none" class="languages language_{{$t->language_id}}">
            <td></td>
            <td>{{$t->slug}}</td>
            <td>{{$t->title}}</td>
            <td>{{$t->subtitle}}</td>
            <td>{{substr($t->content,0,100).'...'}}</td>
            <td>{{ $t->blog->category->translate()->where("language_id",$t->language_id)->first()->title }}</td>
            <td><img style="width: 150px;" src="/images/{{$cat->image}}"> </td>
            <td>
                <button class="btn btn-sm btn-danger" onclick="deleteCat('{{$cat->id}}')">Delete</button>
                <button class="btn btn-sm btn-success"  onclick="edit('{{$cat->id}}')">Edit</button>
            </td>

        </tr>

    @endforeach


@endforeach
</tbody>