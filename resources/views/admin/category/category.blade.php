@extends("admin.layout")


@section('content')
    <section class = "content">
        <div class = "container-fluid">
            <div class = "row">
                <div class = "col-md-6">
                    <div class = "card">
                        <div class = "card-header">
                            <h3 class = "card-title">Bordered Table</h3>
                        </div>
                        <!-- /.card-header -->
                        <div class = "card-body">
                            <select  name = "" class="form-control" style="margin: 20px 0px; width: 100px;display: inline" id = "tabelchange">
                                <option value = "language_1">Az</option>
                                <option value = "language_2">En</option>
                                <option value = "language_3">Ru</option>
                            </select>
                            <button onclick="resetForm()" style="margin: 20px" type = "button" class = "btn btn-primary" data-toggle = "modal"
                                    data-target = "#modal-default">
                                Create Category
                            </button>
                            <div class = "modal fade" id = "modal-default" data-id="0">
                                <div class = "modal-dialog">
                                    <div class = "modal-content">
                                        <div class = "modal-header">
                                            <h4 class = "modal-title" style="width: 100%">Create</h4>
                                            <select name = "" id = "modalchange" class = "form-control" style="width: 100px;">
                                                <option value = "tabs_1">Az</option>
                                                <option value = "tabs_2">En</option>
                                                <option value = "tabs_3">Ru</option>
                                            </select>
                                            {{--<button type = "button" class = "close" data-dismiss = "modal"--}}
                                                    {{--aria-label = "Close">--}}
                                                {{--<span aria-hidden = "true">&times;</span>--}}
                                            {{--</button>--}}
                                        </div>
                                        <form id = "form">
                                            <div class = "modal-body">
                                                <div class = "form-group tabs_1 tabs">
                                                    <label for = "">Title Az</label>
                                                    <input type = "text" name = "name[1]" class = "form-control">
                                                </div>
                                                <div class = "form-group tabs_2 tabs">
                                                    <label for = "">Title En</label>
                                                    <input type = "text" name = "name[2]" class = "form-control">
                                                </div>
                                                <div class = "form-group tabs_3 tabs">
                                                    <label for = "">Title Ru</label>
                                                    <input type = "text" name = "name[3]" class = "form-control">
                                                </div>
                                            </div>
                                            <div class = "modal-footer justify-content-between">
                                                <button type = "button" class = "btn btn-default"
                                                        data-dismiss = "modal">Close
                                                </button>
                                                <button type = "submit" class = "btn btn-primary">Save changes</button>
                                            </div>
                                        </form>
                                    </div>
                                    <!-- /.modal-content -->
                                </div>
                                <!-- /.modal-dialog -->
                            </div>
                            <table class = "table table-bordered" id="dataTable">
                                <thead>
                                <tr>
                                    <th style = "width: 10px">#</th>
                                    <th>Title</th>
                                    <th>options</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($categories as $cat)

                                    @foreach($cat->translate as $t)

                                        <tr style="display: none" class="languages language_{{$t->language_id}}">
                                            <td>{{$cat->id}}</td>
                                            <td>{{$t->title}}</td>
                                            <td>
                                                <button class="btn btn-sm btn-danger" onclick="deleteCat('{{$cat->id}}')">Delete</button>
                                                <button class="btn btn-sm btn-success"  onclick="edit('{{$cat->id}}')">Edit</button>
                                            </td>
                                        </tr>

                                    @endforeach


                                @endforeach
                                </tbody>
                            </table>
                                <ul class="pagination pagination-sm m-0 float-right my-2">
                                </ul>
                        </div>
                    </div>
                    <!-- /.card -->
                    <!-- /.card -->
                </div>
                <!-- /.col -->
                <!-- /.col -->
            </div>
        </div><!-- /.container-fluid -->
    </section>

@endsection

@push("scripts")





    <script>
        $(function () {


            resetForm=()=>{
                $("input").val('')
            }







            $(".pagination").on("click",".page-item",function () {
                var page=$(this).text().trim()



            })










            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': "{{csrf_token()}}"
                }
            });

             edit=(id)=>{
                 $.ajax({
                     url: "/category/show",
                     method:"post",
                     data:{"id":id,"language":$("#modalchange").val()},
                     success: function (e) {
                         $("#modal-default").html(e.view)
                         $(".modal").modal("show")
                         $(".modal").attr("data-id",id)
                     },
                     error: function (e) {
                         console.log(e)
                     }
                 })
            }

            deleteCat=(id)=>{
                 $.ajax({
                     url: "/category/delete",
                     method:"post",
                     data:{"id":id,"language":$("#modalchange").val()},
                     success: function (e) {
                         $("#dataTable").html(e.view)
                         sirala(1)

                         Swal.fire({
                             position: 'top-end',
                             icon: 'success',
                             title: 'Deleted Successfully',
                             showConfirmButton: false,
                             timer: 1500
                         })
                     },
                     error: function (e) {
                         console.log(e)
                     }
                 })
            }

            $(".tabs").hide()
            $(".tabs_1").show()
            $("body").on("change","#modalchange", function () {
                $(".tabs").hide()
                $("."+this.value).show()
            })




            $("body").on("submit","#form", function (e) {
                e.preventDefault();

                var formData=new FormData(this);

                formData.append("id",$(".modal").attr("data-id"))
                formData.append("language",$("#tabelchange").val())

                $.ajax({
                    url: "/category/store",
                    processData: false,
                    contentType: false,
                    cache: false,
                    method:"post",
                    data:formData,
                    success: function (e) {
                        $("#dataTable").html(e.view)

                        $("input").val("")

                        $(".modal").modal("hide")

                        sirala(1)

                        Swal.fire({
                            position: 'top-end',
                            icon: 'success',
                            title: 'Your work has been saved',
                            showConfirmButton: false,
                            timer: 1500
                        })
                    },
                    error: function (e) {
                        var errors=JSON.parse(e.responseText).errors;
                        for(a in errors){
                           for(j in errors[a]){
                               Swal.fire({
                                   position: 'top-end',
                                   icon: 'error',
                                   title: errors[a][j],
                                   showConfirmButton: false,
                                   timer: 1500
                               })
                           }
                        }
                    }
                })

            })



            $(".language_1").show()

            $("#tabelchange").on("change", function () {
                $(".languages").hide()
                $("."+this.value).show()
                sirala(1)
            })












            var goster=5;


            function sirala(page) {


                var lang=$("#tabelchange").val()

                var count=$("tbody tr."+lang).length;

                var page_count=Math.ceil(count/goster);

                var basla=(page-1)*goster;
                var son=page*goster;

                $('.pagination').html('')

                for(var i=1;i<=page_count;i++){
                    $(".pagination").append(` <li class="page-item `+(i==page ? "active" :"")+ ` ">
                                        <a class="page-link " >${i}</a>
                                    </li>`);
                }

                $("."+lang).hide()

                for(var i=basla;i<son;i++){
                    $("."+lang+":eq('"+i+"')").show()
                }


            }

            sirala(1)



            $(".pagination").on("click",".page-item",function () {
                var page=$(this).text().trim()

                sirala(page)
            })








        })
    </script>
@endpush