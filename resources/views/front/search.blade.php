@extends("front.layout")

@section("content")
    <main id = "main" class = "site-main" role = "main">
        <div class = "grid bloggrid">
            @foreach($blogs as $blog)





                    <article>
                        <header class = "entry-header">
                            <h1 class = "entry-title"><a href = "/{{app()->getLocale()}}/blog/{{$blog->slug}}"
                                        rel = "bookmark">{{$blog->title}}</a></h1>
                            <div class = "entry-meta">
                                <span class = "posted-on"><time
                                            class = "entry-date published">April 12, 2016</time></span>
                                <span class = "comments-link"><a href = "#">Leave a comment</a></span>
                            </div>
                            <div class = "entry-thumbnail">
                                <a href = "/{{app()->getLocale()}}/blog/{{$blog->slug}}"><img src = "{{asset('images/'.$blog->blog->image)}}" alt = ""></a>
                            </div>
                        </header>
                        <div class = "entry-summary">

                            <p>
                                {!! $blog->subtitle !!}
                            </p>
                        </div>
                        <footer class = "entry-footer">
					<span class = "cat-links">
					Posted in <a href = "#" rel = "category tag">audio</a>, <a href = "#" rel = "category tag">embed</a>, <a
                                href = "#" rel = "category tag">media</a>
					</span>
                        </footer>
                    </article>


            @endforeach
        </div>
        <div class = "clearfix"></div>
        <nav class = "pagination"></nav>
    </main>
@endsection