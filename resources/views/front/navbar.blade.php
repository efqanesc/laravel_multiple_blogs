<header id="masthead" class="site-header">
    <div class="site-branding">
        <h1 class="site-title"><a href="index.html" rel="home">{{__("message.news")}}</a></h1>
        <style>
            select{
                position: absolute;
                right: 40px;
                top:40px;
            }
        </style>
        <div class="form-group" style="position: absolute;right: 190px">
            <form action = "/{{app()->getLocale()}}/search">
                <input  value="{{request()->search}}" name="search" type = "text" id="text" class="form-control">
                <button type="submit" class="btn btn-block">Axtar</button>
            </form>
        </div>
        <select  name = "" id = "language">
            <option value = "az">Az</option>
            <option value = "en">En</option>
            <option value = "ru">Ru</option>
        </select>
        <h2 class="site-description">{{__('message.logomessage')}}</h2>
    </div>
    <nav id="site-navigation" class="main-navigation">
        <button class="menu-toggle">Menu</button>
        <a class="skip-link screen-reader-text" href="#content">Skip to content</a>
        <div class="menu-menu-1-container">
            <ul id="menu-menu-1" class="menu">
                <li><a href="/{{app()->getLocale()}}">{{__('message.home')}}</a></li>
                <li><a href="/about">{{__('message.about')}}</a></li>

                {{--<li><a href="#">Pages</a>--}}
                    {{--<ul class="sub-menu">--}}
                        {{--<li><a href="portfolio-item.html">Portfolio Item</a></li>--}}
                        {{--<li><a href="blog-single.html">Blog Article</a></li>--}}
                        {{--<li><a href="shop-single.html">Shop Item</a></li>--}}
                        {{--<li><a href="portfolio-category.html">Portfolio Category</a></li>--}}
                    {{--</ul>--}}
                {{--</li>--}}
            </ul>
        </div>
    </nav>
</header>
