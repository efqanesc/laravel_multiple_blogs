<!DOCTYPE html>
<html lang="en-US">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Moschino | Minimalist Free HTML Portfolio by WowThemes.net</title>
    <link rel='stylesheet' href='{{asset("front")}}/css/woocommerce-layout.css' type='text/css' media='all'/>
    <link rel='stylesheet' href='{{asset("front")}}/css/woocommerce-smallscreen.css' type='text/css' media='only screen and (max-width: 768px)'/>
    <link rel='stylesheet' href='{{asset("front")}}/css/woocommerce.css' type='text/css' media='all'/>
    <link rel='stylesheet' href='{{asset("front")}}/css/font-awesome.min.css' type='text/css' media='all'/>
    <link rel='stylesheet' href='{{asset("front")}}/style.css' type='text/css' media='all'/>
    <link rel='stylesheet' href='https://fonts.googleapis.com/css?family=Oswald:400,500,700%7CRoboto:400,500,700%7CHerr+Von+Muellerhoff:400,500,700%7CQuattrocento+Sans:400,500,700' type='text/css' media='all'/>
    <link rel='stylesheet' href='{{asset("front")}}/css/easy-responsive-shortcodes.css' type='text/css' media='all'/>
</head>
<body class="blog">
<div id="page">
    <div class="container">


        @include("front.navbar")
        <!-- #masthead -->
        <div id="content" class="site-content">
            <div id="primary" class="content-area column two-thirds">
                @yield("content")
            </div>
            <div id="secondary" class="column third">
                <div id="sidebar-1" class="widget-area" role="complementary">

                    <aside id="recent-posts-2" class="widget widget_recent_entries">
                        <h4 class="widget-title">{{__('message.categories')}}</h4>
                        <ul>
                            @foreach($categories as $cat)
                            <li>
                                <a href="/{{app()->getLocale().'/category/'.$cat->id}}">{{$cat->title}}</a>
                            </li>
                            @endforeach
                        </ul>
                    </aside>



                </div>
                <!-- .widget-area -->
            </div>
            <!-- #secondary -->
        </div>
        <!-- #content -->
    </div>
    <!-- .container -->
    <footer id="colophon" class="site-footer">
        <div class="container">
            <div class="site-info">
                <h1 style="font-family: 'Herr Von Muellerhoff';color: #ccc;font-weight:300;text-align: center;margin-bottom:0;margin-top:0;line-height:1.4;font-size: 46px;">
                    {{__('message.news')}}
                </h1>
                <a target="blank" href="https://www.wowthemes.net/">Efgan Mesediyev</a>
            </div>
        </div>
    </footer>
    <a href="#top" class="smoothup" title="Back to top"><span class="genericon genericon-collapse"></span></a>
</div>
<!-- #page -->
<script src='{{asset("front")}}/js/jquery.js'></script>
<script src='{{asset("front")}}/js/plugins.js'></script>
<script src='{{asset("front")}}/js/scripts.js'></script>
<script src='{{asset("front")}}/js/masonry.pkgd.min.js'></script>
<script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>

<script>
    $(function () {

        $("select option[value='"+"{{app()->getLocale()}}"+"']").attr("selected","selected")
        $("#language").on("change",function () {

            var lng=this.value
            $.ajax({
                url:"{{route('language')}}",
                type:"POST",
                dataType:"json",
                data:{language:this.value,"_token":"{{csrf_token()}}","url":window.location.href},
                success:function (e) {
                    window.location.href=e.url;


                }
            })

        })
    })
</script>


</body>
</html>