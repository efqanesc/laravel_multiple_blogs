@extends("front.layout")

@section("content")
    <main id = "main" class = "site-main" role = "main">
        <div class = "grid bloggrid">
            @foreach($blogs as $blog)



                @if(count($blog->translate)>0)

                    <article>
                        <header class = "entry-header">
                            <h1 class = "entry-title"><a
                                        href = "/{{app()->getLocale()}}/blog/{{$blog->translate->first()->slug}}"
                                        rel = "bookmark">{{$blog->translate->first()->title}}</a></h1>
                            <div class = "entry-meta">
                                <span class = "posted-on"><time
                                            class = "entry-date published">{{ $blog->created_at->format('Y-M-d') }}</time></span>
                                <span class = "comments-link"><a
                                            href = "#">{{$blog->created_at->diffForHumans()}}</a></span>
                            </div>
                            <div class = "entry-thumbnail">
                                <a href = "/{{app()->getLocale()}}/blog/{{$blog->translate->first()->slug}}"><img
                                            src = "{{asset('images/'.$blog->image)}}" alt = ""></a>
                            </div>
                        </header>
                        <div class = "entry-summary">
                            <p>
                                {!! $blog->translate->first()->subtitle !!}
                            </p>
                        </div>
                        <footer class = "entry-footer"></footer>
                    </article>
                @endif

            @endforeach
        </div>
        <div class = "clearfix"></div>
        <div>
            @if ($blogs->lastPage() > 1)
                <nav class = "pagination">

                       @if($blogs->currentPage() == 1)
                        <a  class = "page-numbers">Previous</a>
                       @else
                        <a class = "page-numbers"
                                href = "{{ $blogs->url($blogs->currentPage()-1) }}">Previous</a>
                        @endif

                    @for ($i = 1; $i <= $blogs->lastPage(); $i++)


                        @if($blogs->currentPage() == $i)
                            <span class = "page-numbers current">{{$i}}</span>
                            @else
                            <a class = "page-numbers" href = "{{ $blogs->url($i) }}">{{ $i }}</a>
                        @endif

                    @endfor
                           @if($blogs->currentPage() == $blogs->lastPage())
                               <a  class = "page-numbers">Next</a>
                               @else
                               <a class = "next page-numbers"
                                       href = "{{ $blogs->url($blogs->currentPage()+1) }}">Next</a>
                               @endif

                </nav>
            @endif
            <ul class = "pagination"></ul>
        </div>
    </main>
@endsection